CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

This module allows a user to add Image Style images into all fields of their
website.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/image_style_embed

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/image_style_embed

REQUIREMENTS
------------

There are no special requirements for this project.

RECOMMENDED MODULES
-------------------

 None

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

 * Add an image style via /admin/config/media/image-styles

 * In your field, enter the following pseudo token:
 [format_image|public://THE_IMAGE_NAME|THE_STYLE_MACHINE_NAME]

TROUBLESHOOTING
---------------

 * None yet. Make sure you enter the right path.

FAQ
---

Q: I don't have any questions.

A: That's the best news I've heard all day!

MAINTAINERS
-----------

Current maintainers:
 * Adam Weiss (greatmatter) - https://www.drupal.org/u/greatmatter

This project has been sponsored by:
 * Great Matter
   We create the software you need most.
   Specializing in Drupal-based systems, Great Matter designs, develops,
   and supports amazing software solutions for the most complicated needs.
   Visit https://www.greatmatter.com for more information.
